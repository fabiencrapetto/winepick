(function($, window, document, undefined) {

	'use strict';

	$(function() {
		String.prototype.capitalize = function() {
			return this.charAt(0).toUpperCase() + this.slice(1);
		};
		var loadPath = window.location.hash,
			pagesDir = '/pages/',
			html = '.html',
			loadPage = function(p) {
				var pTitle = p.length <= 1 ? 'home' : p.substring(1);
				$('#page-content').load(pagesDir + pTitle + html, function() {
					var longTitle = $('.menu[href="' + (pTitle == 'home' ? '/' : p) + '"]').html();
					$('#page-title').text(longTitle);
					$('.navbar-nav li.active').removeClass('active');
					$('#sidebarMenu .menu[href="' + (pTitle == 'home' ? '/' : p) + '"]').parent().addClass('active');
					$('body').scrollTop(0);
					if ($('#sidebarMenu').hasClass('in')) $('.navbar-toggle').click();
				});
			};

		loadPage(loadPath);

		window.onpopstate = function() {
			var popPath = window.location.hash;
			$('.menu').blur();
			loadPage(popPath);
		};
		$('.menu').on('click', function(e) {
			var tar = $(this).attr('href'),
				tarTitle = tar.length <= 1 ? 'home' : tar.substring(1);
			e.stopPropagation();
			e.preventDefault();
			loadPage(tar);
			window.history.pushState('#page-content', tarTitle.capitalize(), tar);
		});

		$(document).on('submit', 'form', function(e) {
			e.preventDefault();
			var $form = $(this),
				thxpage = $form.attr('action');
			$('#page-content').load(pagesDir + thxpage);
		});
	});

})(jQuery, window, document);